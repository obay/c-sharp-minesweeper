﻿namespace MiniSweperX
{
    partial class Sweeper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Enabled = false;
                System.Random rand = new System.Random();
                for (double i = 1; i > 0; i -= 0.001)
                {
                    //BackColor = Color.FromArgb(0, 255 - i, 0);
                    Opacity = i;
                    buttons[rand.Next(0, x)][rand.Next(0, y)].BackColor =
                        System.Drawing.Color.FromArgb(rand.Next(0, 244), rand.Next(0, 244), rand.Next(0, 244));
                    System.Windows.Forms.
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(1);
                }
                System.Threading.Thread.Sleep(10);
                System.Windows.Forms.
                Application.Exit();
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sweeper));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "number_0.bmp");
            this.imageList1.Images.SetKeyName(1, "number_1.bmp");
            this.imageList1.Images.SetKeyName(2, "number_2.bmp");
            this.imageList1.Images.SetKeyName(3, "number_3.bmp");
            this.imageList1.Images.SetKeyName(4, "number_4.bmp");
            this.imageList1.Images.SetKeyName(5, "number_5.bmp");
            this.imageList1.Images.SetKeyName(6, "number_6.bmp");
            this.imageList1.Images.SetKeyName(7, "number_7.bmp");
            this.imageList1.Images.SetKeyName(8, "number_8.bmp");
            this.imageList1.Images.SetKeyName(9, "Bomb.ico");
            // 
            // Sweeper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Name = "Sweeper";
            this.Text = "Sweeper";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ImageList imageList1;

    }
}