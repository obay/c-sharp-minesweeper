﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiniSweperX
{
    public partial class Sweeper : Form
    {
        Color ButoonsColor;
        int Bombsnumber;
        readonly int newGameBombsNumber;//constant Bombs number;
        int text;//set "Text" property to number of Bombs Left;
        int catched;//how many buttons clicked to Know when the player win;
        int seconds = 0;//count the time;
        Button[][] buttons;
        char[][] value;//any value in the "value" array can be {0,1,2,3,4,5,6,7,8,*}; <how many bombs around(0-8) or its abomb(*)>
        Random Rnd = new Random();
        bool Started = true;//to get start the timer only when the user click button for the first time;
        bool helpInStart;//call letsGetStarted() function if true >> last line in the constructor;
        int x, y;// Dimentions
        TableLayoutPanel tableLayout = new TableLayoutPanel();
        public Sweeper(int setx, int sety, int setBombsNumber, Color MyButtonsColor, Color MyBackColor, bool HelpInStart_param)
        {
            StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            x = setx;
            y = sety;
            Bombsnumber = setBombsNumber;
            newGameBombsNumber = Bombsnumber;//remmeber this can not be edited after the constructor;
            ButoonsColor = MyButtonsColor;
            text = Bombsnumber;
            Text = text.ToString();//Bombes left;
            catched = x * y - Bombsnumber;//catched is actualy how many buttons clicked;
            Size = new Size(x * 32 + 50, y * 32);//Form size effectd be buttons number;
            value = new char[x][];
            for (int i = 0; i < x; i++)
            {
                value[i] = new char[y];
            }
            InitializeTable(MyBackColor);
            InitializeButtons();//My Buttons initializer;
            putBombs();
            setNumbers();
            helpInStart = HelpInStart_param;
            if (helpInStart)
                letsGetStarted();
            //test();
        }
        /*
        void setPictures()//replace numbers with Imeges
        {
            for(int i=0;i<x;i++)
                for (int j = 0; j < y; j++)
                {
                    if (value[i][j] == '*')
                        buttons[i][j].BackgroundImage = imageList1.Images[9];
                    else
                        buttons[i][j].BackgroundImage = imageList1.Images[(int)value[i][j] - 48];
                    buttons[i][j].BackgroundImageLayout = ImageLayout.Stretch;
                }
        }
         */
        private void InitializeTable(Color MyBackColor)
        {
            tableLayout.BackColor = MyBackColor;
            tableLayout.Padding = new Padding(0);//set Padding to 5 and see defrence(Padding is the interior spacing in the control ,zero value to save space<like save water>)bla bla bla;
            tableLayout.RowCount = x;
            for (int i = 0; i < x; i++)
                tableLayout.RowStyles.Add(new RowStyle(SizeType.Percent, x));
            tableLayout.Location = new System.Drawing.Point(0, 0);
            tableLayout.ColumnCount = y;
            for (int i = 0; i < y; i++)
                tableLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, y));
            tableLayout.Name = "layout";
            tableLayout.Dock = DockStyle.Fill;
            tableLayout.TabIndex = 0;
            tableLayout.Size = new Size(32 * x, 32 * y);
            tableLayout.GrowStyle = TableLayoutPanelGrowStyle.FixedSize;
            Controls.Add(tableLayout);//this.tableLayout);
        }
        void InitializeButtons()
        {
            buttons = new Button[x][];
            for (int i = 0; i < x; i++)
            {
                buttons[i] = new Button[y];
            }
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    buttons[i][j] = new Button()
                    {
                        ForeColor = Color.Blue,
                        BackColor = ButoonsColor,
                        Dock = DockStyle.Fill,
                        Visible = true,
                        Margin = new Padding(0),
                        Name = i + "," + j,//the name specify the position;
                    };
                    buttons[i][j].MouseDown += new MouseEventHandler(buttons_MouseDown);//MouseDown better than Click evvent because it allows double click{if(e.Clicks==2)DoubleClickDetected();};
                    tableLayout.Controls.Add(buttons[i][j], j, i);//yes j before i because the first is the column and the second is the row
                }
            }
        }
        private void putBombs()
        {
            if (Bombsnumber > x * y)
                throw new OverflowException("Bombs number greater than buttons number");
            while (Bombsnumber > 0)
            {
                int nextx = Rnd.Next(0, x), nexty = Rnd.Next(0, y);
                {
                    if (value[nextx][nexty] != '*')
                    {
                        value[nextx][nexty] = '*';
                        Bombsnumber--;
                    }
                }
            }
        }
        private void setNumbers()
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    int howMany = 0;
                    //these bool values protect the array from "Index was outside the bounds of the array" exeption
                    bool top = (i == 0 ? true : false), down = (i == x - 1 ? true : false),
                         right = (j == y - 1 ? true : false), left = (j == 0 ? true : false);
                    if (value[i][j] != '*')//Bomb!
                    {
                        //1 2 3 
                        //4 V 6
                        //7 8 9
                        if (!top && !left)
                            if (value[i - 1][j - 1] == '*')//1
                                howMany++;
                        if (!top)
                            if (value[i - 1][j] == '*')//2
                                howMany++;
                        if (!top && !right)
                            if (value[i - 1][j + 1] == '*')//3
                                howMany++;
                        if (!left)
                            if (value[i][j - 1] == '*')//4
                                howMany++;
                        if (!right)
                            if (value[i][j + 1] == '*')//6
                                howMany++;
                        if (!down && !left)
                            if (value[i + 1][j - 1] == '*')//7
                                howMany++;
                        if (!down)
                            if (value[i + 1][j] == '*')//8
                                howMany++;
                        if (!down && !right)
                            if (value[i + 1][j + 1] == '*')//9
                                howMany++;

                        value[i][j] = (char)(howMany + 48);//convert from int to char
                    }
                }
            }
        }
        private void buttons_MouseDown(object sender, MouseEventArgs e)
        {
            int X, Y;
            getposition(((Button)sender).Name, out X, out Y);
            if (Started)
            {
                timer1.Start();
                Started = false;
            }
            if (((MouseEventArgs)e).Button == MouseButtons.Left)
            {
                if (buttons[X][Y].Text != "X")
                {
                    if (value[X][Y] == '*')
                    {
                        showBombs();
                        int minutes = seconds / 60;
                        string endingTime = "Time is " + (minutes == 0 ? "0" : minutes + "") + ":" + (seconds - (minutes == 0 ? 0 : minutes * 60)) + "";
                        this.Enabled = false;
                        if ((MessageBox.Show("You lose !!!,Try again?\n" + endingTime, "Opss!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Error))
                            == DialogResult.Yes)
                        {
                            this.Enabled = true;
                            StartNewGame();
                        }
                        else
                            ExitGame();
                    }
                    else
                        if (((Button)sender).Text == "")
                        {
                            ((Button)sender).BackColor = Color.White;
                            if (value[X][Y] != '0')
                                ((Button)sender).Text = value[X][Y].ToString();
                            else
                            {
                                ((Button)sender).Text = " ";
                                button_Left_DoubleClick((Button)sender);
                            }
                            catched--;
                            if (catched == 0)
                            {
                                int minutes = seconds / 60;
                                string endingTime = "Time is " + (minutes == 0 ? "0" : minutes + "") + ":" + (seconds - (minutes == 0 ? 0 : minutes * 60)) + "";
                                this.Enabled = false;
                                if ((MessageBox.Show("You Win !!!,Try again?\n" + endingTime, "Congratulations !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Information))
                                == DialogResult.Yes)
                                {
                                    this.Enabled = true;
                                    StartNewGame();
                                }
                                else
                                    ExitGame();
                            }
                        }
                    if (e.Clicks >= 2)
                        button_Left_DoubleClick((Button)sender);
                }
            }
            else
                if (((MouseEventArgs)e).Button == MouseButtons.Right)
                {
                    if (((Button)sender).Text == "")//&& !clicked[X][Y])
                        addX((Button)sender);
                    else
                        if (((Button)sender).Text == "X")
                            removeX((Button)sender);

                    if (e.Clicks >= 2)
                        button_Right_DoubleClick((Button)sender);
                }
        }
        void button_Left_DoubleClick(Button sender)
        {
            int i, j;
            getposition(((Button)sender).Name, out i, out j);
            if (sender.Text != "" && sender.Text != "X" || value[i][j] == '0')
            {
                //these bool values protect the array from "Index was outside the bounds of the array" exeption
                bool top = (i == 0 ? true : false), down = (i == x - 1 ? true : false),
                    right = (j == y - 1 ? true : false), left = (j == 0 ? true : false);
                int howMany = getHowManyBombsCatchedAround(i, j, top, down, right, left, "X");
                if ((int)howMany.ToString()[0] >= (int)value[i][j])
                {
                    //1 2 3 
                    //4 V 6
                    //7 8 9
                    MouseEventArgs NextE =
                        new MouseEventArgs(MouseButtons.Left, 1, Cursor.Position.X, Cursor.Position.Y, 0);
                    if (!top && !left)
                    {
                        if (buttons[i - 1][j - 1].Text == "")
                        {
                            buttons_MouseDown(buttons[i - 1][j - 1], NextE);
                            Application.DoEvents();
                        }
                    }

                    if (!top)
                    {
                        if (buttons[i - 1][j].Text == "")
                        {
                            buttons_MouseDown(buttons[i - 1][j], NextE);
                            Application.DoEvents();
                        }
                    }

                    if (!top && !right)
                    {
                        if (buttons[i - 1][j + 1].Text == "")
                        {
                            buttons_MouseDown(buttons[i - 1][j + 1], NextE);
                            Application.DoEvents();
                        }
                    }

                    if (!left)
                    {
                        if (buttons[i][j - 1].Text == "")
                        {
                            buttons_MouseDown(buttons[i][j - 1], NextE);
                            Application.DoEvents();
                        }
                    }

                    if (!right)
                    {
                        if (buttons[i][j + 1].Text == "")
                        {
                            buttons_MouseDown(buttons[i][j + 1], NextE);
                            Application.DoEvents();
                        }
                    }

                    if (!down && !left)
                    {
                        if (buttons[i + 1][j - 1].Text == "")
                        {
                            buttons_MouseDown(buttons[i + 1][j - 1], NextE);
                            Application.DoEvents();
                        }
                    }

                    if (!down)
                    {
                        if (buttons[i + 1][j].Text == "")
                        {
                            buttons_MouseDown(buttons[i + 1][j], NextE);
                            Application.DoEvents();
                        }
                    }

                    if (!down && !right)
                    {
                        if (buttons[i + 1][j + 1].Text == "")
                        {
                            buttons_MouseDown(buttons[i + 1][j + 1], NextE);
                            Application.DoEvents();
                        }
                    }
                }
                else
                {
                    Color lastColor = sender.BackColor;
                    for (int R = 254; R >= 120; R--)
                    {
                        sender.BackColor = Color.FromArgb(R, 0, R);
                        Application.DoEvents();
                        System.Threading.Thread.Sleep(3);
                    }
                    sender.BackColor = lastColor;
                }
            }
        }
        void button_Right_DoubleClick(Button sender)
        {
            if (sender.Text == "" || sender.Text == "X")
                return;
            int i, j;
            getposition(((Button)sender).Name, out i, out j);
            bool top = (i == 0 ? true : false), down = (i == x - 1 ? true : false),
                right = (j == y - 1 ? true : false), left = (j == 0 ? true : false);
            //1 2 3 
            //4 V 6
            //7 8 9
            if (!top && !left)
            {
                if (buttons[i - 1][j - 1].Text == "")
                {
                    addX(buttons[i - 1][j - 1]);
                }
            }

            if (!top)
            {
                if (buttons[i - 1][j].Text == "")
                {
                    addX(buttons[i - 1][j]);
                }
            }

            if (!top && !right)
            {
                if (buttons[i - 1][j + 1].Text == "")
                {
                    addX(buttons[i - 1][j + 1]);
                }
            }

            if (!left)
            {
                if (buttons[i][j - 1].Text == "")
                {
                    addX(buttons[i][j - 1]);
                }
            }

            if (!right)
            {
                if (buttons[i][j + 1].Text == "")
                {
                    addX(buttons[i][j + 1]);
                }
            }

            if (!down && !left)
            {
                if (buttons[i + 1][j - 1].Text == "")
                {
                    addX(buttons[i + 1][j - 1]);
                }
            }

            if (!down)
            {
                if (buttons[i + 1][j].Text == "")
                {
                    addX(buttons[i + 1][j]);
                }
            }

            if (!down && !right)
            {
                if (buttons[i + 1][j + 1].Text == "")
                {
                    addX(buttons[i + 1][j + 1]);
                }
            }
        }
        int getHowManyBombsCatchedAround(int i, int j, bool top, bool down, bool right, bool left, string seek)
        {
            int howMany = 0;
            if (!top && !left)
                if (buttons[i - 1][j - 1].Text == seek)//1
                    howMany++;
            if (!top)
                if (buttons[i - 1][j].Text == seek)//2
                    howMany++;
            if (!top && !right)
                if (buttons[i - 1][j + 1].Text == seek)//3
                    howMany++;
            if (!left)
                if (buttons[i][j - 1].Text == seek)//4
                    howMany++;
            if (!right)
                if (buttons[i][j + 1].Text == seek)//6
                    howMany++;
            if (!down && !left)
                if (buttons[i + 1][j - 1].Text == seek)//7
                    howMany++;
            if (!down)
                if (buttons[i + 1][j].Text == seek)//8
                    howMany++;
            if (!down && !right)
                if (buttons[i + 1][j + 1].Text == seek)//9
                    howMany++;
            return howMany;
        }
        private void showBombs()
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (value[i][j] == '*')
                    {
                        buttons[i][j].Text = value[i][j].ToString();
                        buttons[i][j].ForeColor = Color.Lime;
                    }
                }
            }
        }
        void getposition(string s, out int x, out int y)
        {
            int i = 0;
            String num = "";
            while (s[i] >= '0' && s[i] <= '9')
                num += s[i++];
            x = Convert.ToInt32(num);

            i++;
            num = "";
            while (i < s.Length && s[i] >= '0' && s[i] <= '9')
                num += s[i++];
            y = Convert.ToInt32(num);
        }
        void letsGetStarted()//Black eyed peas
        {
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                    if (value[i][j] == '0')
                    {
                        buttons_MouseDown(buttons[i][j],
                            new MouseEventArgs(MouseButtons.Left, 1, Cursor.Position.X, Cursor.Position.Y, 0));
                        i = x; j = y;//break;
                    }
        }
        void addX(Button sender)
        {
            sender.Text = "X";
            sender.ForeColor = Color.Red;
            Text = (--text).ToString();
        }
        void removeX(Button sender)
        {
            sender.Text = "";
            Text = (++text).ToString();
            sender.ForeColor = Color.Blue;
        }
        private void test()
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    buttons[i][j].Text = buttons[i][j].Name;// value[i][j].ToString();
                }
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            seconds++;
        }
        private void StartNewGame()
        {
            //set all variables to it's defaulte values
            Bombsnumber = newGameBombsNumber;
            text = Bombsnumber;
            Text = text.ToString();
            catched = x * y - Bombsnumber;
            Started = true;
            seconds = 0;
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                {
                    buttons[i][j].BackColor = ButoonsColor;
                    buttons[i][j].ForeColor = Color.Blue;
                    buttons[i][j].Text = "";
                    value[i][j] = '\0';
                }
            //new Random Bombs
            putBombs();
            setNumbers();
            if (helpInStart)
                letsGetStarted();

            //Graphic effects
            /*
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    int nextx, nexty;
                    nextx = Rnd.Next(0, x);
                    nexty = Rnd.Next(0, y);
                    if (value[nextx][nexty] != '0' && buttons[nextx][nexty].Text == "")
                    {
                        buttons[nextx][nexty].BackColor = Color.FromArgb(Rnd.Next(0, 255), Rnd.Next(0, 255), 0);
                        Application.DoEvents();
                        buttons[nextx][nexty].BackColor = ButoonsColor;
                    }
                }
            }*/
        }
        void ExitGame()
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }
    }//end class
}//end namespace
