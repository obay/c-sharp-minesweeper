﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MiniSweperX
{
    public partial class UserInput : Form
    {
        Color ButtonsColor = Color.Gray, GameBackColor = Color.Tan;
        public UserInput()
        {
            InitializeComponent();
            buttonButtonsColor.ForeColor = ButtonsColor;
            buttonBackColor.ForeColor = GameBackColor;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                buttonButtonsColor.ForeColor = ButtonsColor = colorDialog1.Color;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                buttonBackColor.ForeColor = GameBackColor = colorDialog1.Color;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Sweeper Game = new Sweeper((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, ButtonsColor, GameBackColor,checkBox1.Checked);
            Game.Show();
            this.Hide();
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown3.Value = decimal.Round((numericUpDown1.Value * numericUpDown2.Value * 15) / 81);//recommended value
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown3.Value = decimal.Round((numericUpDown1.Value * numericUpDown2.Value * 15) / 81);//recommended value
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            buttonButtonsColor.ForeColor = ButtonsColor = Color.Gray;
            buttonBackColor.ForeColor = GameBackColor = Color.Tan;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            numericUpDown1.Value = numericUpDown1.Maximum;
            numericUpDown2.Value = numericUpDown2.Maximum;
        }

        private void button3_Click_1(object sender, EventArgs e)
        {

            numericUpDown1.Value = numericUpDown1.Minimum;
            numericUpDown2.Value = numericUpDown2.Minimum;
        }
    }
}
